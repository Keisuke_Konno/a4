import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** Quaternions. Basic operations. */
public class Quaternion {

   // TODO!!! Your fields here!
   // Define threshold 0.00001
   public static final double THRESHOLD = 0.000001;
   private final double a;
   private final double b;
   private final double c;
   private final double d;

   /** Constructor from four double values.
    * @param a real part
    * @param b imaginary part i
    * @param c imaginary part j
    * @param d imaginary part k
    */
   public Quaternion (double a, double b, double c, double d) {
      // TODO!!! Your constructor here!
      this.a = a;
      this.b = b;
      this.c = c;
      this.d = d;

   }

   //My additional method
   private boolean containZero(Quaternion q){
      if(Math.abs(0.0 - q.getRpart()) < Quaternion.THRESHOLD)return true;
      if(Math.abs(0.0 - q.getIpart()) < Quaternion.THRESHOLD)return true;
      if(Math.abs(0.0 - q.getJpart()) < Quaternion.THRESHOLD)return true;
      if(Math.abs(0.0 - q.getKpart()) < Quaternion.THRESHOLD)return true;

      return false;
   }

   /** Real part of the quaternion.
    * @return real part
    */
   public double getRpart() {
      return this.a; // TODO!!!
   }

   /** Imaginary part i of the quaternion. 
    * @return imaginary part i
    */
   public double getIpart() {
      return this.b; // TODO!!!
   }

   /** Imaginary part j of the quaternion. 
    * @return imaginary part j
    */
   public double getJpart() {
      return this.c; // TODO!!!
   }

   /** Imaginary part k of the quaternion. 
    * @return imaginary part k
    */
   public double getKpart() {
      return this.d; // TODO!!!
   }

   /** Conversion of the quaternion to the string.
    * @return a string form of this quaternion: 
    * "a+bi+cj+dk"
    * (without any brackets)
    */
   @Override
   public String toString() {
      double[] arr = new double[4];
      arr[0] = this.a;
      arr[1] = this.b;
      arr[2] = this.c;
      arr[3] = this.d;

      StringBuilder buffer = new StringBuilder(String.valueOf(arr[0]));

      for(int i=1; i<4; i++){
         if(arr[i] < 0){
            if(arr[i] == (int)arr[i]){
               buffer.append(String.valueOf((int)arr[i]));
            }else{
               buffer.append(String.valueOf(arr[i]));
            }
         }else if(arr[i] > 0){

            if(arr[i] == (int)arr[i]){
               buffer.append("+").append(String.valueOf((int)arr[i]));
            }else{
               buffer.append("+").append(String.valueOf(arr[i]));
            }
         }

         if(arr[i] != 0){
            switch (i) {
               case 1 : buffer.append("i"); break;
               case 2 : buffer.append("j"); break;
               case 3 : buffer.append("k"); break;
            }
         }
      }
      System.out.println(buffer.toString());
      return buffer.toString(); // TODO!!!
   }

   //Personal method
   private static double trasnslateExpo(String s) {
      String[] arr = new String[2];
      arr[0] = s.substring(0,s.indexOf("e"));
      if(s.indexOf("e") != s.length()){
         arr[1] = s.substring(s.indexOf("e")+1,s.length());
      }

      double result = 0;
      if(!arr[1].isEmpty()){
         result = Double.parseDouble(arr[0])*Math.pow(10,Double.parseDouble(arr[1]));
      }else{
         result = Double.parseDouble(arr[0])*10;
      }

      return result;
   }
   /** Conversion from the string to the quaternion. 
    * Reverse to <code>toString</code> method.
    * @throws IllegalArgumentException if string s does not represent 
    *     a quaternion (defined by the <code>toString</code> method)
    * @param s string of form produced by the <code>toString</code> method
    * @return a quaternion represented by string s
    */
   public static Quaternion valueOf (String s) {
      // if there is Exponent value in the formula
      double a=0,b=0,c=0,d=0;
      System.out.println("ORIGINAL"+s);
      if(s.contains("e")){
         Pattern exponent = Pattern.compile("[+-]?\\d+(\\.\\d+)+([e][+-]?\\d+)?");
         Matcher findExpo = exponent.matcher(s);
         while(findExpo.find()) {
            String find = new String(findExpo.group(0));
            System.out.println("FIND B:" + find);
            switch (s.charAt(s.indexOf(find) + find.length())){
               case 'i': b = trasnslateExpo(find);break;
               case 'k': c = trasnslateExpo(find);break;
               case 'j': d = trasnslateExpo(find);break;
               default: a += trasnslateExpo(find);break;
            }
         }



         s = String.valueOf(a);
         if(a == (int)a){
            s = String.valueOf((int)a);
         }else{
            s = String.valueOf(a);
         }

         if(b > 0){
            if(b == (int)b){
               s = s + "+" +String.valueOf((int)b)+"i";
            }else{
               s = s + "+" +String.valueOf(b)+"i";
            }
         }else if(b < 0){
            if(b == (int)b){
               s = s +String.valueOf((int)b)+"i";
            }else{
               s = s +String.valueOf(b)+"i";
            }
         }
         if(c > 0){
            if(c == (int)c){
               s = s + "+" +String.valueOf((int)c)+"j";
            }else{
               s = s + "+" +String.valueOf(c)+"j";
            }
         }else if(c < 0){
            if(c == (int)c){
               s = s +String.valueOf((int)c)+"j";
            }else{
               s = s +String.valueOf(c)+"j";
            }
         }
         if(d > 0){
            if(d == (int)d){
               s = s + "+" +String.valueOf((int)d)+"k";
            }else{
               s = s + "+" +String.valueOf(d)+"k";
            }
         }else if(d < 0){
            if(d == (int)d){
               s = s +String.valueOf((int)d)+"k";
            }else{
               s = s +String.valueOf(d)+"k";
            }
         }
         System.out.println("AfterChange"+s);
      }

      String copy = new String(s);
      boolean realMinus = false;
      if(copy.charAt(0)=='-'){
         copy = copy.substring(1,copy.length());
         realMinus = true;
      }

      String[] nums = copy.split("[+-]");
      System.out.println(Arrays.toString(nums));
      String[] ijk = {"i","j","k"};

      for (int i=0; i<nums.length; i++) {
         //Add op
         if(i==0 && realMinus){
            nums[0] = "-"+nums[0];
         }else if(i > 0){
            int op = copy.indexOf(nums[i])-1;
            if(copy.charAt(op) == '+' || copy.charAt(op) == '-') {
               nums[i] = copy.charAt(op) + nums[i];
            }else{
               //Error: invalid operator
               throw new RuntimeException("Error operator:"+copy.charAt(op)+" in "+s);
            }
         }

         //Add nums with op
         for (String value : ijk) {
            if(nums[i].contains(value)){
               nums[i] = nums[i].replaceAll(value, "");
               switch (value) {
                  case "i" : b = Double.parseDouble(nums[i]); break;
                  case "j" : c = Double.parseDouble(nums[i]); break;
                  case "k" : d = Double.parseDouble(nums[i]); break;
                  //Error : invalid input
                  default : throw new RuntimeException("Error input:"+nums[i]+" in " +s);
               }
            }else{
               a = Double.parseDouble(nums[0]);
            }
         }
      }
      System.out.println(Arrays.toString(nums));

      Quaternion result = new Quaternion(a,b,c,d);
      if(!result.toString().equals(s)){
         //Error : Original should be the same even after toString
         throw new IllegalArgumentException("Not represent Quaternion\n"+"Original:"+s+"\nThis:"+result.toString());
      }

      return result; // TODO!!!
   }

   /** Clone of the quaternion.
    * @return independent clone of <code>this</code>
    */
   @Override
   public Object clone() throws CloneNotSupportedException {

      return new Quaternion(this.a, this.b, this.c, this.d); // TODO!!!
   }

   /** Test whether the quaternion is zero. 
    * @return true, if the real part and all the imaginary parts are (close to) zero
    */
   public boolean isZero() {
      return Math.abs(0.0 - this.a) < Quaternion.THRESHOLD && Math.abs(0.0 - this.b) < Quaternion.THRESHOLD && Math.abs(0.0 - this.c) < Quaternion.THRESHOLD && Math.abs(0.0 - this.d) < Quaternion.THRESHOLD;// TODO!!!
   }

   /** Conjugate of the quaternion. Expressed by the formula 
    *     conjugate(a+bi+cj+dk) = a-bi-cj-dk
    * @return conjugate of <code>this</code>
    */
   public Quaternion conjugate() {
      return new Quaternion(this.a, -1*this.b,-1*this.c,-1*this.d); // TODO!!!
   }

   /** Opposite of the quaternion. Expressed by the formula 
    *    opposite(a+bi+cj+dk) = -a-bi-cj-dk
    * @return quaternion <code>-this</code>
    */
   public Quaternion opposite() {
      return new Quaternion(-this.a,-this.b,-this.c,-this.d); // TODO!!!
   }

   /** Sum of quaternions. Expressed by the formula 
    *    (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
    * @param q addend
    * @return quaternion <code>this+q</code>
    */
   public Quaternion plus (Quaternion q) {
      double a = this.a + q.getRpart();
      double b = this.b + q.getIpart();
      double c = this.c + q.getJpart();
      double d = this.d + q.getKpart();
      return new Quaternion(a,b,c,d);// TODO!!!
   }

   /** Product of quaternions. Expressed by the formula
    *  (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
    *  (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
    * @param q factor
    * @return quaternion <code>this*q</code>
    */
   public Quaternion times (Quaternion q) {
      double para1 = this.a*q.getRpart()-this.b*q.getIpart()-this.c*q.getJpart()-this.d*q.getKpart();
      double para2 = this.a*q.getIpart()+this.b*q.getRpart()+this.c*q.getKpart()-this.d*q.getJpart();
      double para3 = this.a*q.getJpart()-this.b*q.getKpart()+this.c*q.getRpart()+this.d*q.getIpart();
      double para4 = this.a*q.getKpart()+this.b*q.getJpart()-this.c*q.getIpart()+this.d*q.getRpart();

      return new Quaternion(para1,para2,para3,para4);
   }

   /** Multiplication by a coefficient.
    * @param r coefficient
    * @return quaternion <code>this*r</code>
    */
   public Quaternion times (double r) {
      return new Quaternion(this.a*r,this.b*r,this.c*r,this.d*r); // TODO!!!
   }

   /** Inverse of the quaternion. Expressed by the formula
    *     1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) + 
    *     ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
    * @return quaternion <code>1/this</code>
    */
   public Quaternion inverse() {
      Quaternion zero = new Quaternion(this.a,this.b,this.c,this.d);
      if(zero.isZero())throw new RuntimeException("These are zeros");

      double a = this.a;
      double b = this.b;
      double c = this.c;
      double d = this.d;
      double tmp = a*a+b*b+c*c+d*d;
      return new Quaternion(a/(tmp),(-1*b)/(tmp),(-1*c)/(tmp),(-1*d)/(tmp)); // TODO!!!
   }

   /** Difference of quaternions. Expressed as addition to the opposite.
    * @param q subtrahend
    * @return quaternion <code>this-q</code>
    */
   public Quaternion minus (Quaternion q) {
      Quaternion tmp = q.opposite();
      double a = this.a + tmp.getRpart();
      double b = this.b + tmp.getIpart();
      double c = this.c + tmp.getJpart();
      double d = this.d + tmp.getKpart();
      return new Quaternion(a,b,c,d); // TODO!!!
   }

   /** Right quotient of quaternions. Expressed as multiplication to the inverse.
    * @param q (right) divisor
    * @return quaternion <code>this*inverse(q)</code>
    */
   public Quaternion divideByRight (Quaternion q) {
      if(q.containZero(q))throw new RuntimeException("Contain division by zero:"+q);
      return this.times(q.inverse());
   }

   /** Left quotient of quaternions.
    * @param q (left) divisor
    * @return quaternion <code>inverse(q)*this</code>
    */
   public Quaternion divideByLeft (Quaternion q) {
      if(q.containZero(q))throw new RuntimeException("Contain division by zero:"+q);
      return q.inverse().times(this); // TODO!!!
   }
   
   /** Equality test of quaternions. Difference of equal numbers
    *     is (close to) zero.
    * @param qo second quaternion
    * @return logical value of the expression <code>this.equals(qo)</code>
    */
   @Override
   public boolean equals (Object qo) {
      if(qo instanceof Quaternion){
         double a = ((Quaternion) qo).getRpart();
         double b = ((Quaternion) qo).getIpart();
         double c = ((Quaternion) qo).getJpart();
         double d = ((Quaternion) qo).getKpart();
         return Math.abs(a - this.a) < 0.000001 && Math.abs(b - this.b) < 0.000001 && Math.abs(c - this.c) < 0.000001 && Math.abs(d - this.d) < 0.000001;// TODO!!!
      }
      return false;
   }

   /** Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
    * @param q factor
    * @return dot product of this and q
    */
   public Quaternion dotMult (Quaternion q) {
      System.out.println("THIS:"+this);
      Quaternion result = this.times(q.conjugate());
      result = result.plus(q.times(this.conjugate()));
      result = result.times(0.5);
      return result;// TODO!!!
   }

   /** Integer hashCode has to be the same for equal objects.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      return Objects.hash(this.a,this.b,this.c,this.d); // TODO!!!
   }

   /** Norm of the quaternion. Expressed by the formula 
    *     norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
    * @return norm of <code>this</code> (norm is a real number)
    */
   public double norm() {
      return Math.sqrt(Math.pow(this.a,2)+Math.pow(this.b,2)+Math.pow(this.c,2)+Math.pow(this.d,2)); // TODO!!!
   }

   /** Main method for testing purposes. 
    * @param arg command line parameters
    */
   public static void main (String[] arg) {
      System.out.println("LAB4 --START--");
      System.out.println(valueOf("-2.1e-2-3.2e-1i-4.3e+2j-5.4e2k"));
      //System.out.println(valueOf("-2.1e-2-3.2e-1i-4.3e+2j-5.4e2kk"));
      //System.out.println(valueOf("-2.1e-2-3.2e-1i-4.3e+2j-5.4e2k+"));
      System.out.println("LAB4 --END--");
      //System.out.println("Result:"+valueOf("-2.0e-3-4.0e-1i-5.0e-2j-6.0e-3k"));
   }
   }
// end of file
